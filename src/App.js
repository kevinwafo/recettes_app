import React from 'react'
import PropTypes from 'prop-types'
// CSS
import './App.css'

// components
import Header from './components/Header'
import Admin from './components/Admin'
import Card from './components/Card'
import ColorProvider from './components/Color'

import withFirebase from './hoc/withFirebase'

const App = ({
  recettes,
  ajouterRecette,
  majRecette,
  supprimerRecette,
  chargerExemple,
  match }) => {

  const cards = Object.keys(recettes).map(key => <Card key={key} details={recettes[key]} /> )
    return (
      <ColorProvider>
        <div className='box'>
          <Header pseudo={match.params.pseudo} />
          <div className='cards'>
              {cards}
          </div>
          <Admin 
            ajouterRecette={ajouterRecette}
            majRecette={majRecette} 
            supprimerRecette={supprimerRecette}
            recettes={recettes}
            pseudo={match.params.pseudo}
            chargerExemple={chargerExemple} 
          />
        </div>
      </ColorProvider>
    )
  }

App.propTypes = {
  recettes: PropTypes.object.isRequired,
  ajouterRecette: PropTypes.func.isRequired,
  majRecette: PropTypes.func.isRequired,
  supprimerRecette: PropTypes.func.isRequired,
  chargerExemple: PropTypes.func.isRequired,
  match: PropTypes.object.isRequired,
}

const WrappedComponent = withFirebase(App)

export default WrappedComponent
