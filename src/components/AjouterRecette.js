import React, { Component } from 'react'

class AjouterRecette extends Component {

    state = {...this.initialState}

    initialState = {
        nom: '',
        image: '',
        ingredients: '',
        instructions: ''
    }

    handleChange = even => {
        const {name, value} = even.target
        this.setState({[name]: value})
    } 

    handleSubmit = even => {
        even.preventDefault()
        const recette = {...this.state}

        this.props.ajouterRecette(recette)

        this.setState({...this.initialState})

    }

    render() {
        return (
            <div className="card">
                <form className="admin-form ajouter-recette" onSubmit={this.handleSubmit}>
                    <input type="text" name="nom" value={this.state.nom} onChange={this.handleChange} placeholder="Nom de la recette"/>
                    <input type="text" name="image" value={this.state.image} onChange={this.handleChange} placeholder="Nom de l'image"/>
                    <textarea name="ingredients" value={this.state.ingredients} onChange={this.handleChange} rows="3" placeholder="Liste des ingrédients"/>
                    <textarea name="instructions" value={this.state.instructions} onChange={this.handleChange} rows="15" placeholder="Liste des instructions"/>
                    <button type='submit'>+ Ajouter une recette</button>
                </form>
            </div>
        )
    }
}

export default AjouterRecette
